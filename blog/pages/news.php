
<?php 
    $select_posts = "SELECT * FROM posts";
    $result = mysqli_query($connect, $select_posts);
?>

<?php foreach($result as $post) {?>
    <div class="post__container">
        <h2><?=$post['title']?></h2>
        <h5><?=$post['add_date']?></h5>
        <div class=""><img class="post__img" src="<?=$post['image']?>"></div>
        <p><?=$post['text']?></p>
    </div>
<?php } ?>