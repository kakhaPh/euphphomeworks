<?php
    session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>KP Blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
   <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
    </style>
</head>
<body>
<?php include_once 'admin/database/dbconnect.php';?>
<?php
    include "blocks/header.php";
    include "blocks/topnav.php";
?>
<div class="container mt-1">
    <div class="row">
        <?php
            include "blocks/leftnav.php";
        ?>

        <div class="col-sm-8">
            <?php include "blocks/content.php"?>
        </div>
    </div>
</div>
<?php
    include "blocks/footer.php";
?>
</body>
</html>
