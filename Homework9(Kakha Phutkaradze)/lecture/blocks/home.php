<h1>Home</h1>
<hr>
<table class="data-table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>LastName</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $select_query ="SELECT * FROM users ORDER BY Id ASC";
            $result = mysqli_query($connect, $select_query);
            if($result){
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <tr>
                        <td><?=$row['Id']?></td>
                        <td><?=$row['Name']?></td>
                        <td><?=$row['Lastname']?></td>
                        <td><?=$row['Address']?></td>
                    </tr>
                    <?php 
                }
            }else {
                echo "The table is empty";
            }
        }
        ?>
    </tbody>
    <tfoot>
        <td colspan="4">Num records = <?=mysqli_num_rows($result)?></td>
    </tfoot>
</table>

<script src="scripts/script.js"></script>
