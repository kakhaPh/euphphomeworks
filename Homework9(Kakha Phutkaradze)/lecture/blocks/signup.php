<?php 
    function captcha(){
        $captcha = "";
        for($i=0; $i<5; $i++){
           $captcha .= rand(0, 9)." ";
        }
        return $captcha;
    }
    $captcha = captcha();
     
    function check_captcha($captcha){ 
        if(str_replace(' ', '', $_POST['real_captcha'])==$captcha){
           return "";
        }else{
           return "<span style='color: red;'>The captcha isn't correct!!</span>";
        }
    }
?>

<?php
    $error = "";
    if(isset($_POST["signup"])) {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $re_password = $_POST["re_password"];

        // Password validation
        $uppercase = preg_match('@[A-Z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        // Check if email exists
        $selectemail = "SELECT email FROM admin WHERE email = '".$email."'";
        $emailCheck = mysqli_query($connect, $selectemail);

        if($password==$re_password){
            $password = sha1($salte.$password);
            $insert = "INSERT INTO admin(email, password) VALUES('$email', '$password')";
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error = "Invalid email format!";
            }elseif(mysqli_num_rows($emailCheck)) {
                $error = "This email is already exists!";
            }else if(empty($password)){
                $error = "Please enter password!";
            }else if(!$uppercase) {
                $error = "Password should include at least one upper case letter one number!";
            }else if(!$number){
               $error = "Password should include one number!";
            }else if(!$specialChars){
               $error = "Password should include one special character @!#$%^&&*. !";
            }else if(strlen($password) < 8){
               $error = $error = "Password should be at least 8 characters in length!";
            }else if(!mysqli_query($connect, $insert)) {
                die("error with mysql insert query!!");
            }else{
                header("location: index.php?nav=signin");
            }
        }else {
            $error = "Passwords are not equal!";
        }
    }
    
?>
<h1>Sign up</h1>
<hr>
<form method="POST">
    <input type="email" name="email" placeholder="email...">
    <br><br>
    <input type="password" name="password" placeholder="password...">
    <br><br>
    <input type="password" name="re_password" placeholder="repeat password">
    <br><br>
    <label><b>Re-captcha</b></label><br><br>
    <div class="captcha">
        <span><?=$captcha?></span>
        <input type="hidden" name="real_captcha" value="<?=$captcha?>"> 
    </div>
    <div>
        <input type="text" name="captcha">
    </div>
    <div class="result">
        <span> <?php if(isset($_POST['captcha'])){ echo check_captcha($_POST['captcha']); } ?> </span>
    </div><br>
    <input type="submit" value="Sign Up" name="signup">
    <br><br>
    <span style='color: red;'><?=$error?></span>
</form>