<h1>SELECT</h1>
<hr>
<table class="data-table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>LastName</th>
            <th>Address</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $select_query ="SELECT * FROM users ORDER BY Id ASC";
            $result = mysqli_query($connect, $select_query);
            if($result){
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <tr>
                        <td><?=$row['Id']?></td>
                        <td><?=$row['Name']?></td>
                        <td><?=$row['Lastname']?></td>
                        <td><?=$row['Address']?></td>
                        <td><a href="?nav=edit&&id=<?=$row['Id']?>">edit</a></td>
                        <td><a href="?nav=delete&&id=<?=$row['Id']?>" title="delete" onclick="return confirm('Are you sure you want to delete this item')">Delete</a></td>
                    </tr>
                    <?php 
                }
            }else {
                echo "The table is empty";
            }
        }
        ?>
    </tbody>
    <tfoot>
        <td colspan="6">Num records = <?=mysqli_num_rows($result)?></td>
    </tfoot>
</table>

<script src="scripts/script.js"></script>
