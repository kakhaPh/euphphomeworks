<?php 
    session_start();
    require "mysql/db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL Quaries One to one</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <?php include "blocks/nav.php"?>
        <div class="content">
            <?php
                if(isset($_GET["nav"])&&$_GET["nav"]=="select" && isset($_SESSION['email'])){
                    include "blocks/select.php";
                }elseif(isset($_GET["nav"])&& $_GET["nav"]=="insert" && isset($_SESSION['email'])){
                    include "blocks/insert.php";
                }elseif(isset($_GET["nav"])&& $_GET["nav"]=="delete" && isset($_SESSION['email'])){
                    include "blocks/delete.php";
                }elseif(isset($_GET["nav"])&& $_GET["nav"]=="edit" && isset($_SESSION['email'])){
                    include "blocks/edit.php"; 
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="signup"){
                    include "blocks/signup.php";
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="signin"){
                    include "blocks/signin.php";
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="signout"){
                    unset($_SESSION['email']);
                    header("location: index.php");
                }else{
                    include "blocks/home.php";
                }
            ?>           
        </div>
    </div>

</body>
</html>