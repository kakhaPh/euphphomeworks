<?php include_once 'blocks/header.php'?> 

<!-- First work -->
<div class="for-flex-forms">
    <form action="payroll.php" method="GET">
        <label for="name">სახელი:</label><br>
        <input type="text" name="name"><br>
        <label for="lname">გვარი:</label><br>
        <input type="text" name="lname"><br>
        <label for="position">დაკავებული თანამდებობა:</label><br>
        <input type="text" name="position"><br>
        <label for="salary">ხელფასის რაოდენობა:</label><br>
        <input type="text" name="salary"><br>
        <button type="submit" name="btn">Submit</button>
    </form>  

    <!-- Second work -->
    <form action="marks.php" method="POST">
        <label for="name">სტუდენტის სახელი:</label><br>
        <input type="text" name="name"><br>
        <label for="lname">გვარი:</label><br>
        <input type="text" name="lname"><br>
        <label for="course">კურსი:</label><br>
        <input type="text" name="course"><br>
        <label for="term">სემესტრი:</label><br>
        <input type="text" name="term"><br>
        <label for="studyCourse">სასწავლო კურსი</label>
        <input type="text" name="studyCourse">
        <label for="marks">მიღებული ნიშანი</label>
        <input type="number" name="marks">
        <button type="submit" name="btn">Submit</button>
    </form>

    <!-- Third work -->
    <form action="quiz.php" method="POST">
        <label class="Question1">Question1: Lorem ipsum dolor sit amet consectetur adipisicing elit?</label>
        <div class="flex">
            <input type="radio" name="Question1" value="1"> Answer 1
            <input type="radio" name="Question1" value="2"> Answer 2
        </div>

        <label class="Question2">Question1: Lorem ipsum dolor sit amet consectetur adipisicing elit?</label>
        <div class="flex">
            <input type="radio" name="Question2" value="1"> Answer 1
            <input type="radio" name="Question2" value="2"> Answer 2
        </div>

        <label class="Question3">Question1: Lorem ipsum dolor sit amet consectetur adipisicing elit?</label>
        <div class="flex">
            <input type="radio" name="Question3" value="1"> Answer 1
            <input type="radio" name="Question3" value="2"> Answer 2
        </div>
        
        <label class="Question4">Question1: Lorem ipsum dolor sit amet consectetur adipisicing elit?</label>
        <div class="flex">
            <input type="radio" name="Question4" value="1"> Answer 1
            <input type="radio" name="Question4" value="2"> Answer 2
        </div>

        <label class="Question5">Question1: Lorem ipsum dolor sit amet consectetur adipisicing elit?</label>
        <div class="flex">
            <input type="radio" name="Question5" value="1"> Answer 1
            <input type="radio" name="Question5" value="2"> Answer 2
        </div>

        <button type="submit" name="btn">Submit</button>
    </form>
</div>

<?php include_once 'blocks/footer.php'?>