<?php
    $error = '';
    if(isset($_POST['submit'])) {
        $userData = $_POST['userData'];
        if(empty($userData)){
            $error = 'Please fill the Gap';
        } else {
            $time = date("Y.m.d.h.i.s");
            $CreateFile = fopen("files/$time.txt", "w") or die("There is a problem");
            fwrite($CreateFile, $userData);

            fclose($CreateFile);
        }
    }
?>