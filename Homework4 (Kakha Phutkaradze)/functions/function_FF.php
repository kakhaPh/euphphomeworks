<?php
    $foldername = isset($_POST['createFolder']) ? $_POST['createFolder'] : '';
    $subfolder = isset($_POST['createsubFolder']) ? $_POST['createsubFolder'] : '';
    $createFileMain = isset($_POST['createFileMain']) ? $_POST['createFileMain'] : '';
    $createFileSUB = isset($_POST['createFileSUB']) ? $_POST['createFileSUB'] : '';
    $successfully = '';
    $error = '';
    $error2 = '';

    if(isset($_POST['create'])) {

        if(empty($foldername)){
            $error = 'Please fill the Gap';
        }else {
            mkdir(''.$foldername);
            $successfully = '<span id="successfuly">Main Folder Successfuly created</span><br>';
        }

        if(!empty($subfolder)) {
            mkdir("$foldername/".$subfolder);
        }

        if(!empty($createFileMain)) {
            $TEXT = 'HELLO IN MAIN FOLDER';
            $CreateFile = fopen("$foldername/$createFileMain.txt", "w") or die("There is a problem");
            fwrite($CreateFile, $TEXT);
            
            fclose($CreateFile);
        }elseif(!empty($createFileSUB)) {
            $TEXT = 'HELLO IN SUB FOLDER';
            $CreateFile = fopen("$foldername/$subfolder/$createFileSUB.txt", "w") or die("There is a problem");
            fwrite($CreateFile, $TEXT);

            fclose($CreateFile);
        }
        
    }

    if(isset($_POST['deletebtn'])){
        if(empty($_POST['delete'])){
            $error2 = 'Please fill the Gap';
        }else {
            $delete = isset ($_POST['delete']) ? $_POST['delete'] : '';
            if(file_exists(''.$foldername.'/'.$delete)){
                if(is_dir(''.$foldername.'/'.$delete)){
                    rmdir(''.$foldername.'/'.$delete);
                }
                $successfully = '<span id="successfuly">Main Folder Successfuly Deletede</span><br>';
            }else{
                $failed = '<span id="failed">Cant Find this file</span><br>';
            }
        }
    }
?>