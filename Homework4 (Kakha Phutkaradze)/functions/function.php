<?php
    $validemail = '';
    $validpass = '';
    $strength = '';

    if(isset($_POST['submit'])){
        $flname = $_POST['flname'];
        $email = $_POST['email'];
        $pass = $_POST['psw'];
        $pass2 = $_POST['psw-repeat'];

        if(!preg_match('/[@]/', $email)){
            $validemail = 'Please enter correct E-mail address!';
        }
        
        if($pass !== $pass2){
            $validpass = 'please enter similar passwords!';
        }
        
        else if (strlen($pass)>7 && preg_match("/[0-9]/", $pass)) {
            $strength = "<span style='color: green;'>This is Normal password</span>";
        }else {
            $strength = "This is Bad password";
        }
    }

    function captcha(){
        $captcha = "";
        for($i=0; $i<5; $i++){
           $captcha .= rand(0, 9)." ";
        }
        return $captcha;
    }
    $captcha = captcha();
     
    function check_captcha($captcha){ 
        if(str_replace(' ', '', $_POST['real_captcha'])==$captcha){
           return "<span style='color: green;'>The captcha is correct!!</span>";
        }else{
           return "It is not a correct!!";
        }
    }
    
?>

