<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>PHP</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <?php
      include "functions.php";
   ?>
   <div class="container">
      <form action="" method="post">
         <input type="text" name="name_folder"> <button name="create_folder">Create Folder</button>
      </form>
   </div>
   <div class="container">
      <div>
         <?php
            if(isset($_GET["sub"])){
               $drives = scandir('./Drive/'.$_GET["sub"]);
               $drive_path = "Drive > ".$_GET["sub"];
            }else{
               $drives = scandir('./Drive');
               $drive_path = "Drive";
            }
            print_r($drives);
         ?>
            <h2><?=$drive_path?></h2>
         <?php
            
            for($i=2; $i<count($drives); $i++){
         ?> 
            <div class="item">
               <div>
                  <a href="?sub=<?=$drives[$i]?>"><?=$drives[$i]?></a>
               </div>
               <form action="" method="post">
                  <input type="hidden" name="folder_name" value="<?=$drives[$i]?>">
                  <input type="submit" name="delete_folder" value="delete">
               </form>
            </div>
            <?php
               }
            ?>
      </div>
   </div>
</body>
</html>
