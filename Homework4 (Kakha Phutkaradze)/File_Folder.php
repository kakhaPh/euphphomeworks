<?php include_once 'blocks/header.php';?>
<?php require 'functions/function_FF.php'?>

<h2 class="title">LikeDrive</h2>
<form action="" method="POST">
    <div class="container">
        <label class="lbl">Create Folder / File </label><span style="color: red">*<?=$error?></span>
        <input type="text" name="createFolder" placeholder="Enter Folder name..."><br><?=$successfully?>
        <input type="text" name="createsubFolder" placeholder="Sub folder name...">
        <input type="text" name="createFileMain" placeholder="Create Main File / Enter File name...">
        <input type="text" name="createFileSUB" placeholder="Create Sub File / Enter File name...">
        <button type="submit" name="create" class="createbtn">Create</button>
    </div>
</form>

<h2 class="title">Delete Created files</h2>
<form action="" method="POST">
    <div class="container">
        <label class="lbl">Delete Folder / File </label><span style="color: red">*<?=$error2?></span>
        <input type="text" name="delete" placeholder="Removable Folder/File...">
        <button type="submit" name="deletebtn" class="deletebtn">Delete</button>
    </div>
</form>

<?php include_once 'blocks/footer.php';?>