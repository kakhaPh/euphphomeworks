<?php include_once 'blocks/header.php';?>

<?php require 'functions/files.php'?>
<h1 class="title">File</h1>

<form action="" method="POST">
    <div class="container">
        <Label>Your File Data</Label><span style="color: red">*<?=$error?></span>
        <textarea type="text" name="userData" placeholder="Enter information..."></textarea>
        <button type="submit" name="submit" class="createbtn">Create</button>
    </div>
</form>

<?php include_once 'blocks/footer.php';?>