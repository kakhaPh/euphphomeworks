<?php include 'blocks/header.php';?>


<?php require 'functions/function.php';?>

<form action="" method="POST">
  <div class="container">
    <h1 class="title">Register</h1>
    <hr>
    <label><b>Name, Lastname</b></label>
    <input type="text" placeholder="Name, Lastname..." name="flname">
    <label><b>Email</b></label><span style="color: red">*<?=$validemail?></span>
    <input type="text" placeholder="Enter Email..." name="email" id="email">
    <label><b>Password</b></label><span style="color: red">*<?=$validpass?><?=$strength?></span>
    <input type="password" placeholder="Enter Password..." name="psw" id="psw">
    <label><b>Repeat Password</b></label><span style="color: red">*<?=$validpass?><?=$strength?></span>
    <input type="password" placeholder="Repeat Password..." name="psw-repeat" id="psw-repeat">
      <div class="captcha">
        <span><?=$captcha?></span>
        <input type="hidden" name="real_captcha" value="<?=$captcha?>"> 
      </div>
      <label><b>Re-captcha</b></label>
      <div>
        <input type="text" name="captcha">
      </div>
      <div class="result">
        <span> <?php if(isset($_POST['captcha'])){ echo check_captcha($_POST['captcha']); } ?> </span>
      </div>
    <hr>
    <button type="submit" name="submit" class="registerbtn">Register</button>
  </div>
</form>


<?php include 'blocks/footer.php';?>