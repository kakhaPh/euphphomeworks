<!-- მეორე დავალება -->
<?php
$error = '';
$scan = '';
if(isset($_POST['submit'])) {
    $ka = $_POST['ka'];
    $en = $_POST['en'];

    if(strlen($ka) < 3 || strlen($ka) > 49 || 
       strlen($en) < 3 || strlen($en) > 49) {
        $error = "Incorrect Word!";
    }else {
        $lexicon = fopen('files/lexicon.txt', 'a+');
        fwrite($lexicon, "\n".$en.'--'.$ka.',');
        fclose($lexicon);
    }

    $lexicon = fopen('files/lexicon.txt', 'r');
    $scan = fread($lexicon, filesize('files/lexicon.txt'));
    fclose($lexicon);

}
?>