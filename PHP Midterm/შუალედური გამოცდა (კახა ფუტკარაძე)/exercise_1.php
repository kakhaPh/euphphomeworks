<!-- პირველი დავალება -->
<?php include_once 'blocks/header.php';?>

<?php require 'functions/function_1.php';?>
<h1 class="title">Files</h1>

<form action="" method="POST" enctype="multipart/form-data">
    <div class="container">
        <Label>Your File Data</Label><span style="color: red">*<?=$error?></span>
        <textarea type="text" name="userData" placeholder="Enter information..."></textarea>

        <button type="submit" name="submit" class="createbtn">Create</button>
    </div>
</form>

<?php include_once 'blocks/footer.php';?>