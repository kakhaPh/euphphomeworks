<?php
    $error = '';
    $Files = scandir('files/');
    $randomNum = rand(10,100);

    if(isset($_POST['submit'])) {
        $userData = $_POST['userData'];
        if(empty($userData)){
            $error = 'Please fill the Gap';
        } else {
            $CreateFile = fopen("files/".$randomNum.$userData.".txt", "w") or die("There is a problem");
            fwrite($CreateFile, $randomNum);

            fclose($CreateFile);
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1 - Kakha Phutkaradze</title>
</head>
<body>
    
<h1 class="title">Files</h1>

<form action="" method="POST" enctype="multipart/form-data">
    <div class="container">
        <Label>Your File Data</Label><span style="color: red">*<?=$error?></span>
        <textarea type="text" name="userData" placeholder="Enter information..."></textarea>

        <button type="submit" name="submit" class="createbtn">Create</button>
    </div>
</form>

</body>
</html>