<?php 
   $error_size = "";
   $error_extension = "";
   $path = "storage/";
   $max_size = 100 * 1024 * 1024;

    if(isset($_POST["uploadbtn"])){
      echo "<h3>File uploaded</h3>";
      $name = $_FILES["uploaded_file"]["name"];
      $size = $_FILES["uploaded_file"]["size"];
      
      $path .= date("d.m.Y").$_FILES["uploaded_file"]["name"];

      if(strtolower(pathinfo($path)['extension']) != "png" &&
        strtolower(pathinfo($path)['extension']) != "jpg" &&
        strtolower(pathinfo($path)['extension']) != "gif"){
        $error_extension = "extension of file is not correct!!!";
      }

      if($size > $max_size){
        $error_size = "Maximum file size is 100Mb!!!";
      }


      if(empty($error_size) && empty($error_extension)){
        move_uploaded_file($_FILES["uploaded_file"]["tmp_name"], $path);
      }
    }

    echo $error_size;
    echo "<br>";
    echo $error_extension;

    // remove 
    if(isset($_POST["delete_folder"])){
        unlink("storage/".$_POST['folder_name']);
    }

?>