<?php include_once 'blocks/header.php';?>
    <?php
        if(isset($_GET["sub"])){
            $storage = scandir('storage/'.$_GET["sub"]);
            $path = "storage > ".$_GET["sub"];
        }else{
            $storage = scandir('storage/');
            $path = "storage";
        }
    ?>
    <div class="container">
        <form enctype="multipart/form-data" method="POST">
            <input type="file" name="uploaded_file" accept=".png, .jpg, .gif">
            <br><br>
            <input type="submit" value="Upload File" name="uploadbtn">
        </form>
    </div>

    <div class="container">
        <?php require 'functions/upload.php';?>
    </div>

    <div class="container">
        <h2><?=$path?></h2>
        <?php for($i=1; $i<count($storage); $i++){  ?> 
            <div class="item">
                <div>
                    <a href="?sub"><?=$storage[$i]?></a>
                </div>
                <form method="post">
                    <input type="hidden" name="folder_name" value="<?=$storage[$i]?>">
                    <input type="submit" name="delete_folder" value="delete">
                </form>
            </div>
        <?php } ?>
    </div>
<?php include_once 'blocks/footer.php';?>