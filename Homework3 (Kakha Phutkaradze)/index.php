<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework3 (Kakha Phutkaradze)</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- Exercise 1 -->
    <?php
        echo "<h4>მაგალითი 1</h4>";
        function Table() {
            echo "<table>";
            for($i=0; $i<11; $i++) {
                echo "<tr>";
                for($j=0; $j<11; $j++) {
                    $randomNum = rand(10,99);
                    echo "<td> $randomNum </td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }Table();
    ?>

    <!-- Exercise 2  -->
    <?php
        echo "<h4>მაგალითი 2</h4>";
        if(isset($_POST['Submit'])) {
        function createTable($Row,$Column,$min, $max) {
            echo "<table>";
            for($i=0; $i<$Row; $i++) {
                echo "<tr>";
                for($j=0; $j<$Column; $j++) {
                    $randomNum = rand($min,$max);
                    echo "<td> $randomNum </td>";
                }
                echo "</tr>";
            }
            echo "</table>"."<br>";
        }createTable($_POST['Row'],$_POST['Column'],$_POST['min'],$_POST['max']);
    }
    ?>

    <form action="" method="POST">
        <input type="number" name="Row" placeholder="Row...">
        <input type="number" name="Column" placeholder="Column...">
        <input type="number" name="min" placeholder="Random from...">
        <input type="number" name="max" placeholder="max...">
        <button name="Submit">Submit</button>
    </form>

    <!-- Exercise 3  -->
    <?php
    function generate($length = 10) {
        echo "<h4>მაგალითი 3</h4>";
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    $myRandomString = generate(5);
    echo $myRandomString;
    ?>

    <!-- Exercise 5 -->
    
    <?php
        echo "<h4>მაგალითი 5</h4>";
        if(isset($_POST['countLetters'])){
            $count = $_POST['letters'];
            echo strlen($count);
        }
    ?>
    <form action="" method="POST">
        <input type="number" name="letters">
        <button name="countLetters">Submit</button>
    </form>

    <!-- Exercise 9 -->

    <?php
    function checkPassword($pwd, &$errors) {
        $errors_init = $errors;
    
        if (strlen($pwd) < 8) {
            $errors[] = "Password too short!";
        }
    
        if (!preg_match("#[0-9]+#", $pwd)) {
            $errors[] = "Password must include at least one number!";
        }
    
        if (!preg_match("#[a-zA-Z]+#", $pwd)) {
            $errors[] = "Password must include at least one letter!";
        }     
    
        return ($errors == $errors_init);
    }
    ?>


    <!-- Exercise 10 -->
    <?php
    echo "<h4>მაგალითი 10</h4>";
    function UrlChecker(){
        $URL = $_SERVER['REQUEST_URI'];
        if(preg_match('/[0-9]/', $URL)){
           echo "<span class='yes'>URL with numbers</span>";
        }else{
            echo "<span class='no';>URL WITHOUT numbers</span>";
        }
    }UrlChecker();
    ?>
</body>
</html>