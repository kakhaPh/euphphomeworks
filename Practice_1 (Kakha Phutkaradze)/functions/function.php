<?php 

$today = date("Y.m.d");

if(isset($_POST['submit'])){
    $errorFname = $errorLname = $errorPersId = $errorAddress = $errorMobile = '';
    $fname = $_POST['fname']; 
    $lname = $_POST['lname']; 
    $persid = $_POST['persid']; 
    $address = $_POST['address']; 
    $year = $_POST['Year'];
    $month = $_POST['Month'];
    $date = $_POST['Date'];
    $birthdate = $year.".".$month.".".$date;
    $mobile = $_POST['mobile']; 
    $moreinf = $_POST['moreinf']; 
    $errorFname = CheckFname($fname);
    $errorLname = Checklname($lname);
    $errorPersId = Checkpersid($persid);
    $errorAddress = Checkaddress($address);
    $errorMobile = CheckMobile($mobile);

}
    function CheckFname($fname) {
        if(empty($fname)){
            return 'გთხოვთ შეავსოთ ველი!';
        }elseif(strlen($fname)<2 ){
            return 'მინიმუმ 2 სიმბოლო!';
        }elseif(strlen($fname)>20) {
            return 'მაქსიმუმ 20 სიმბოლო!';
        }
    }

    function Checklname($lname) {
        if(empty($lname)){
            return 'გთხოვთ შეავსოთ ველი!';
        }elseif(strlen($lname)<2 ){
            return 'მინიმუმ 2 სიმბოლო!';
        }elseif(strlen($lname)>20) {
            return 'მაქსიმუმ 20 სიმბოლო!';
        }
    }

    function Checkpersid($persid) {
        if(empty($persid)) {
            return 'გთხოვთ შეავსოთ ველი!';
        }elseif(strlen($persid) != 11 || is_numeric($persid)) {
            return 'გთხოვთ მიუთითეთ სწორი პ/ნ!';
        }
    }

    function Checkaddress($address) {
        if(empty($address)) {
            return 'გთხოვთ შეავსოთ ველი!';
        }elseif (strlen($address)>70){
            return 'მაქსიმუმ 70 სიმბოლო!';
        }
    }

    function CheckMobile($mobile) {
        if(empty($mobile)) {
            return 'გთხოვთ შეავსეთ ველი!';
        }elseif(!is_numeric($mobile)) {
            return 'გთხოვთ მიუთითეთ მხოლოდ ციფრები!';
        }
    }

?>