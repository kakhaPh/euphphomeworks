<?php require_once "mysql/connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL</title>
    <link rel="stylesheet" href="styles/styles.css">
</head>
<body>
    <div class="content">
        <h1>MySQL</h1>
        <hr>
        <!-- #1 Menu -->
        <table>
        <?php
            $select_query = "SELECT `Text`, `Title` FROM menu";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>1) menu ცხრილიდან გამოიტანეთ მხოლოდ პირველი ჩანაწერის Text, Title ველების მნიშვნელობები.</p>";
                if(mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                ?>
                <tr>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Title']?></td>
                </tr>
                <?php 
                }   
            }
        ?>
        </table>

        <!-- #2 Menu -->
        <table>
        <?php
            $select_query = "SELECT `Text`, `Title` FROM menu";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>2)menu ცხრილიდან გამოიტანეთ ყველა ჩანაწერის Text, Title ველების მნიშვნელობები.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Title']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #3 Menu -->
        <table>
        <?php
            $select_query = "SELECT * FROM menu WHERE id = 2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>3) menu ცხრილიდან გამოიტანეთ იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის id=2.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #4 Menu -->
        <table>
        <?php
            $select_query = "SELECT * FROM menu WHERE id >= 2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>4) menu ცხრილიდან გამოიტანეთ იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის id>=2.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #5 Menu -->
        <table>
        <?php
            $select_query = "SELECT * FROM menu WHERE id <= 4";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>5) menu ცხრილიდან გამოიტანეთ იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის id<=4.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #6 Menu -->
        <table>
        <?php
            $select_query = "SELECT * FROM menu WHERE Title = 'თამაშები' OR Title = 'ფილმები'";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>6) menu ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა Title= “ფილმები” ან Title= „თამაშები “; </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #7 Menu -->
        <table>
        <?php
            $select_query = "SELECT * FROM menu WHERE Title = 'მუსიკები' AND id > 3";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise'>7) menu ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა Title= “მუსიკები” და Id>3; </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #1 Users -->
        <table>
        <?php
            $select_query = "SELECT `Age`, `Date`, `Reg_Date`, `Gender` FROM users WHERE id <= 3";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise2'>1) users ცხრილიდან გამოიტანეთ პირველი 3 ჩანაწერის Age, Date, Reg_Date, Gender ველების მნიშვნელობები. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #2 Users -->
        <table>
        <?php
            $select_query = "SELECT * FROM users WHERE id <= 2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise2'>2) users ცხრილიდან გამოიტანეთ პირველი 2 ჩანაწერის ყველა ველის მნიშვნელობები. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Name']?></td>
                    <td><?=$row['Lastname']?></td>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Password']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #3 Users -->
        <table>
        <?php
            $select_query = "SELECT * FROM users WHERE id > 1 AND id < 4";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise2'>3) users ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა id>1 და id<4. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Name']?></td>
                    <td><?=$row['Lastname']?></td>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Password']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #4 Users -->
        <table>
        <?php
            $select_query = "SELECT * FROM users WHERE id < 2 OR id >= 4";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise2'>4) users ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა id<2 ან Id>=4. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Name']?></td>
                    <td><?=$row['Lastname']?></td>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Password']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #6-#7-#8 Users -->
        <table>
        <?php
            // $select_query = "SELECT * FROM users WHERE `Date` = '2014-07-04'";
            // $select_query = "SELECT * FROM users WHERE `Date` > '2014-07-04'";
            $select_query = "SELECT * FROM users WHERE `Date` > '2002-06-04' AND `Date` < '2010-07-04'";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                // echo "<p class='exercise2'>5) users ცხრილიდან გამოიტანეთ იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის Date = 2014-07-04.. </p>";
                // echo "<p class='exercise2'>6) users ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა Date > 2014-07-04. </p>";
                echo "<p class='exercise2'>7) users ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა Date > 2014-06-04 და Date < 2014-07-04.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Name']?></td>
                    <td><?=$row['Lastname']?></td>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Password']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }else{
                    echo 'Table is empty';
                }   
            }
        ?>
        </table>

        
        <!-- #8 Users -->
        <table>
        <?php
            $select_query = "SELECT * FROM users WHERE Age >= 10 AND Age <= 18";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise2'>8) users ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა Age => 10 და Age < =18. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Name']?></td>
                    <td><?=$row['Lastname']?></td>
                    <td><?=$row['Age']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Reg_Date']?></td>
                    <td><?=$row['Password']?></td>
                    <td><?=$row['Gender']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>

        <!-- #1 Data -->
        <table>
        <?php
            $select_query = "SELECT * FROM `Data` WHERE id <=7 AND `type` = 2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise3'>1) data ცხრილიდან გამოიტანეთ იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა id<=7 და type=2. </p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Type']?></td>
                    <td><?=$row['Photo']?></td>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Autor']?></td>
                    <td><?=$row['Description']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>

        <!-- #2-#3-#4 Data -->
        <table>
        <?php
            $select_query = "SELECT * FROM `Data` ORDER BY id DESC LIMIT 5";
            // $select_query = "SELECT * FROM `Data` ORDER BY id DESC LIMIT 10 DESC";
            // $select_query = "SELECT * FROM `Data` WHERE (id % 2) = 0";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                echo "<p class='exercise3'>2) data ცხრილიდან გამოიტანეთ ბოლო 5 ჩანაწერის ყველა ველის მნიშვნელობები. </p>";
                // echo "<p class='exercise3'>3) data ცხრილიდან გამოიტანეთ ბოლო 10 ჩანაწერის ყველა ველის მნიშვნელობები შებრუნებული რიგით.</p>";
                // echo "<p class='exercise3'>4) data ცხრილიდან გამოიტანეთ ლუწი id-ის მქონე ჩანაწერების ყველა ველის მნიშვნელობები.</p>";
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Date']?></td>
                    <td><?=$row['Type']?></td>
                    <td><?=$row['Photo']?></td>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Autor']?></td>
                    <td><?=$row['Description']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                </tr>
                <?php 
                    }
                }   
            }
        ?>
        </table>
    </div>
</body>
</html>