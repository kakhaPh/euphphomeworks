<?php include 'blocks/header.php'?>

<?php

$error = '';
$name = $email = $website = $comment = $gender = '';

if(isset($_POST['submit'])){
    $name =  $_POST['name'];
    $email = $_POST['email'];
    $website = $_POST['website'];
    $comment = $_POST['comment'];
    $gender = $_POST['gender'];

    if(empty($name) && empty($email) && empty($website) && empty($comment) && empty($gender)){
        $error = "field is empty";
    }
}

?>

<form method="post">
 
    <div>
        <h3>Name</h3>
        <input type="text" value="<?php echo $name?>" name="name" placeholder="name">
        <span style="color: red;"><?=$error?><span>
    </div>
    <div>
        <h3>Email</h3>
        <input type="text" value="<?php echo $email?>" name="email" placeholder="email">
        <span style="color: red;"><?=$error?><span>
    </div>
    <div>
        <h3>Website</h3>
        <input type="text" value="<?php echo $website?>" name="website" placeholder="website">
        <span style="color: red;"><?=$error?><span>
    </div>
    <div>
        <h3>Comment</h3>
        <textarea name="comment" value="<?php echo $comment?>" cols="30" rows="10" placeholder="comment"></textarea>
        <span style="color: red;"><?=$error?><span>
    </div>
 
    <div>

    <h3>gender: </h3>
    Male<input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="<?php echo $gender?>">
    Female<input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="<?php echo $gender?>">
    Other<input type="radio" name="gender" <?php if (isset($gender) && $gender=="other") echo "checked";?> value="<?php echo $gender?>">  
    <span style="color: red;"><?=$error?><span>

    </div>
    
    <button name="submit">submit</button>

</form>

    <div>
        <h3>Your input:</h3>
        <?php
            echo $name."<br>";
            echo $email."<br>";
            echo $website."<br>";
            echo $comment."<br>";
            echo $gender."<br>";
        ?>
    </div>

<?php include 'blocks/footer.php'?>