<?php include_once 'blocks/header.php';?>

<?php
$cars = array(
    array("Make"=>"Toyota", 
		"Model"=>"Corolla", 
		"Color"=>"White", 
		"Mileage"=>24000, 
		"Status"=>"Sold"),

    array("Make"=>"Toyota", 
		"Model"=>"Camry", 
		"Color"=>"Black", 
		"Mileage"=>56000, 
		"Status"=>"Available"),

    array("Make"=>"Honda", 
		"Model"=>"Accord", 
		"Color"=>"White", 
  		"Mileage"=>15000, 
		"Status"=>"Sold") 
    );
    print_r($cars)
?>
<?php

echo "<table border='1'>";
echo "<tr>";
echo "<th>Make</th>";
echo "<th>Model</th>";
echo "<th>Color</th>";
echo "<th>Mileage</th>";
echo "<th>Status</th>";
echo "</tr>";
foreach($cars as $car){
    echo "<tr>";
    echo "<td>".$car['Make']."</td>";
    echo "<td>".$car['Model']."</td>";
    echo "<td>".$car['Color']."</td>";
    echo "<td>".$car['Mileage']."</td>";
    echo "<td>".$car['Status']."</td>";
    echo "</tr>";
}

?>

<?php include_once 'blocks/footer.php';?>
