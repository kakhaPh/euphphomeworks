<?php include_once 'blocks/header.php'?>

<?php 
        $error = "*";
        $user_value = "";
        if(isset($_POST['send'])) {
            $user_value = $_POST['answer'];
            if(empty($user_value)) {
                $error = "This field is empty!!!";
            }
        }
?>
<div class="big_container">
    <form method="POST">
        <label>სახელი</label><br>
        <input type="text" name="name"> <span style="color: red;"><?=$error?></span><br>
        <label>გვარი</label><br>
        <input type="text" name="lname"> <span style="color: red;"><?=$error?></span><br>
    
        <label>უპასუხეთ შეკითხვებს</label>
        <?php
            $answers = [];
            $questions = [
                ["sqrt(64)="],
                ["(a+b)^2="],
                ["sqrt(1024)="],
                ["97+93="],
                ["(a+b)^3="],
            ];
            for($i = 0; $i < count($questions); $i++) {
         ?>
    
        <h3><?=$questions[$i][0]?></h3>
        <input type="text" name="answer[<?=$i?>]" placeholder="პასუხი..."> <span style="color: red;"><?=$error?></span>
    
        <?php } ?>
        <br><br>
        <button name="send">Submit</button>
    </form>
</div>

<?php include_once 'blocks/footer.php'?>