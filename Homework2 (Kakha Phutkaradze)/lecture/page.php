<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $arr1 = [4, 5, 6, 9];
        $arr2 = [
            [3, 5, 7],
            [6, 9, 2],
            [2, 3],
            [3, 7, 1, 5],
            [6, 7, 9, 12]
        ];
        echo "<pre>";
        print_r($arr2);
        echo "</pre>";
        echo "<hr>";
        echo $arr2[1][1];
    ?>
</body>
</html>