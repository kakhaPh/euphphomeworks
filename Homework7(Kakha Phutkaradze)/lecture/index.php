<?php require "mysql/db.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL Quaries One to one</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <?php include "blocks/nav.php"?>
        <div class="content">
            <?php
                if(isset($_GET["nav"])&&$_GET["nav"]=="select"){
                    include "blocks/select.php";
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="insert"){
                    include "blocks/insert.php";
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="delete"){
                    include "blocks/delete.php";
                }elseif(isset($_GET["nav"])&&$_GET["nav"]=="edit"){
                    include "blocks/edit.php";
                }else{
                    include "blocks/home.php";
                }
            ?>           
        </div>
    </div>

</body>
</html>