<?php
    $id = $_GET['id'];
    $select_for_edit = "SELECT * FROM users WHERE id='$id'";
    if(!mysqli_query($connect, $select_for_edit)){
        die("Error with mysql select query!!!");
    }else{
        $result = mysqli_query($connect, $select_for_edit);
        $record = mysqli_fetch_assoc($result);
    }
?>

<h1>Edit</h1>
<hr>
<form method="POST">
    <input type="text" name="Name" placeholder="Name..." value="<?=$record['Name']?>">
    <br><br>
    <input type="text" name="Lastname" placeholder="Lastname..." value="<?=$record['Lastname']?>">
    <br><br>
    <textarea name="Address" cols="24" rows="7"><?=$record['Address']?></textarea>
    <br><br>
    <input type="hidden" name="id" value="<?=$record['Id']?>">
    <input type="submit" value="EDIT" name="edit">
</form>
<?php 
    $error = "";
    if(isset($_POST["edit"])) {
        $name = $_POST["Name"];
        $lastname = $_POST["Lastname"];
        $address = $_POST["Address"];
        $id = $_POST['id'];
        $edit = "UPDATE users SET Name='$name', Lastname='$lastname', Address='$address' WHERE id='$id'";
        
        if(empty($name)){
            $error = "Name is required";
        }else if(strlen($name) < 2 || strlen($name) > 25){
            $error ="Please enter correct name";
        }else if(empty($lastname)){
            $error = "lastname is required";
        }else if(strlen($lastname) < 2 || strlen($lastname) > 50){
            $error ="Please enter correct lastname";
        }else if(empty($address)){
            $error = "address is required";
        }else if(!mysqli_query($connect, $edit)){
            die("Error with mysql Update query");
        }else if(empty($error)){
            header("location: index.php?nav=select");
        }
    }
?>