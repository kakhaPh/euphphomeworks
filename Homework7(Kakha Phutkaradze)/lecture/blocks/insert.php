<?php 
    $error = "";
    $name = $lastname = $address = "";
    if(isset($_POST["submit"])) {
        $name = $_POST["Name"];
        $lastname = $_POST["Lastname"];
        $address = $_POST["Address"];
        $insert = "INSERT INTO users(Name, Lastname, Address) VALUES('$name', '$lastname', '$address')";
        
        if(empty($name)){
            $error = "Name is required";
        }else if(strlen($name) < 2 || strlen($name) > 25){
            $error ="Please enter correct name";
        }else if(empty($lastname)){
            $error = "lastname is required";
        }else if(strlen($lastname) < 2 || strlen($lastname) > 50){
            $error ="Please enter correct lastname";
        }else if(empty($address)){
            $error = "address is required";
        }else if(!mysqli_query($connect, $insert)){
            die("Error with mysql insert query");
        }else if(empty($error)){
            header("location: index.php?nav=select");
        }

    }
?>
<h1>INSERT</h1>
<hr>
<form method="POST">
    <input type="text" name="Name" placeholder="Name..." value="<?=$name?>">
    <br><br>
    <input type="text" name="Lastname" placeholder="Lastname..." value="<?=$lastname?>">
    <br><br>
    <textarea name="Address" cols="24" rows="7"><?=$address?></textarea>
    <br><br>
    <input type="submit" value="INSERT" name="submit">
    <h3 class="error"><?=$error?></h3>
</form>
