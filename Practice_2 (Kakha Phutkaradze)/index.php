<?php require "MySQL/db.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practice_2 (Kakha Phutkaradze)</title>
    <link rel="stylesheet" href="styles/styles.css">
</head>
<body>
    <!-- Header Section -->
    <?php include_once "blocks/header.php"?>

    <!-- Content Section -->
    <section class="py-5">
        <?php
            if(isset($_GET["menu"]) && $_GET["menu"]=="about"){
                include "";
            }elseif(isset($_GET["menu"]) && $_GET["menu"]=="contact"){
                include "";
            }else {
                include "content/home.php";
            }
        ?>
    </section>
    
    <!-- Footer Section -->
    <?php include_once "blocks/footer.php"?>

    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>