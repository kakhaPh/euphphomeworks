<?php
    session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 2</title>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
    <h1>Page 2</h1>
    <?php include "menu.php"?>
    <hr>
    <?php
        echo "<br>";
        echo "Session x2 = ".$_SESSION["x2"];
        echo "<br>";
        echo "Session x3 = ".$_SESSION["x3"];
        echo "<br>";
        echo "Session x4 = ".$_SESSION["x4"];
    ?>
</body>
</html>