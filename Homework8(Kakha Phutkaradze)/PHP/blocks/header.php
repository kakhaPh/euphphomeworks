<header>
    <div class="logo">
        <a href="index.php" class="logo_ttl">KP</a>
    </div>

    <div class="log-reg">
        <a href="?menu=login" class="log-reg-a">Login</a>
        <b class="log-slash">/</b>
        <a href="?menu=register" class="log-reg-a">Register</a>
    </div>
    
    <nav>
        <ul class="header_ul">
            <li class="header_li"> <a class="header_links" id="<?php if(!isset($_GET["menu"])){echo "active";}?>" href="index.php">Home</a> </li>
            <li class="header_li"> <a class="header_links" id="<?php if(isset($_GET["menu"]) && $_GET["menu"]=="companies"){echo "active";}?>" href="index.php?menu=companies">Companies</a> </li>
            <li class="header_li"> <a class="header_links" id="<?php if(isset($_GET["menu"]) && $_GET["menu"]=="vacancies"){echo "active";}?>" href="index.php?menu=vacancies">vacancies</a> </li>
        </ul>
    </nav>
</header>