<?php
    $error = "";
    $name = $address = $year = "";

    if(isset($_POST["submit"])) {
        $name = $_POST["name"];
        $address = $_POST["address"];
        $year = $_POST["year"];

        $addCompany = "INSERT INTO companies(name, address, year) VALUES('$name','$address','$year')";
        
        if(empty($name)) {
            $error = "Name is required!";
        }else if(strlen($name) < 2 || strlen($name) > 50) {
            $error = "Please enter correct Company name";
        }else if(empty($address)) {
            $error = "Address is required";
        }else if(empty($year)) {
            $error = "Year is required!";
        }else if (!mysqli_query($connect, $addCompany)) {
            die ("Error with mysql add query");
        }else if(empty($error)){
            header("location: index.php?menu=companies");
        }
    }
?>
<h2 class="title">Add Company</h2>

<div class="form_cont">
    <form method="POST">
        <input type="text" name="name" placeholder="Company Name..." value="<?=$name?>">
        <br><br>
        <textarea name="address" cols="24" rows="7"><?=$address?></textarea>
        <br><br>
        <input type="number" name="year" placeholder="Year..." value="<?=$year?>">
        <br><br>
        <input type="submit" name="submit" value="add Company" >
        <br><br>
        <span class="warning"><?=$error?></span>
    </form>
</div>