<h2 class="title">Login</h2>

<div class="form_cont">
    <form action="" method="POST">
        <label><b>Email</b></label><span style="color: red">*</span><br>
        <input type="text" placeholder="Enter Email..." name="email" id="email"><br><br>
        <label><b>Password</b></label><span style="color: red">*</span><br>
        <input type="password" placeholder="Enter Password..." name="pwd" id="psw"><br><br>
        <br><br>
        <?php if($_SESSION['attempt'] >= 3){?>
            <label><b>Re-captcha</b></label><br><br>
            <div class="captcha">
                <span><?=$captcha?></span>
                <input type="hidden" name="real_captcha" value="<?=$captcha?>"> 
            </div>
            <div>
                <input type="text" name="captcha">
            </div>
            <div class="result">
                <span> <?php if(isset($_POST['captcha'])){ echo check_captcha($_POST['captcha']); } ?> </span>
            </div><br>
        <?php } ?>
        <input type="submit" name="submit" value="Login">
    </form>
</div>