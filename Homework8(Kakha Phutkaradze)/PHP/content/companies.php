<h2 class="title">Companies</h2>
<table class="data-table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Year</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $select_query ="SELECT * FROM companies ORDER BY id ASC";
            $result = mysqli_query($connect, $select_query);

            if($result){
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <tr>
                        <td><?=$row['id']?></td>
                        <td><?=$row['name']?></td>
                        <td><?=$row['address']?></td>
                        <td><?=$row['year']?></td>
                        <td><a href="?menu=edit&&id=<?=$row['id']?>">edit</a></td>
                        <td><a href="?menu=delete&&id=<?=$row['id']?>" title="delete" onclick="return confirm('Are you sure you want to delete this item')">Delete</a></td>
                    </tr>
                    <?php 
                }
            }else {
                echo "The table is empty";
            }
        }
        ?>
    </tbody>
    <tfoot>
        <td colspan="6">Num records = <?=mysqli_num_rows($result)?></td>
    </tfoot>
</table>

<a href="?menu=addcompany" class="addComp"><button class="addComp">Add companies</button></a>