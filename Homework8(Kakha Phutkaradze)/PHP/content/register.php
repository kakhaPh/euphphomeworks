<h2 class="title">Register</h2>

<?php include_once "functions/reg_funct.php";?>
<div class="form_cont">
    <form action="" method="POST">
        <label><b>Name, Lastname</b></label><br>
        <input type="text" placeholder="Name, Lastname..." name="flname"><br><br>
        <label><b>Email</b></label><span style="color: red">*</span><br>
        <input type="text" placeholder="Enter Email..." name="email" id="email"><br><br>
        <label><b>Password</b></label><span style="color: red">*</span><br>
        <input type="password" placeholder="Enter Password..." name="pwd" id="psw"><br><br>
        <label><b>Repeat Password</b></label><span style="color: red">*</span><br>
        <input type="password" placeholder="Repeat Password..." name="pwd-repeat" id="psw-repeat"><br><br>
        <label><b>Re-captcha</b></label><br><br>
        <div class="captcha">
            <span><?=$captcha?></span>
            <input type="hidden" name="real_captcha" value="<?=$captcha?>"> 
        </div>
        <div>
            <input type="text" name="captcha">
        </div>
        <div class="result">
            <span> <?php if(isset($_POST['captcha'])){ echo check_captcha($_POST['captcha']); } ?> </span>
        </div><br>
        <input type="submit" name="submit" value="Register">
    </form>
</div>