<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP small project</title>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
    <?php require "db/db.php"?>
    <!-- Header -->
    <?php include_once 'blocks/header.php'?>
    <!-- Content -->
    <div class="container">
        <?php
            if(isset($_GET["menu"]) && $_GET["menu"]=="companies"){
                include "content/companies.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="delete"){
                include "functions/delete.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="edit"){
                include "functions/edit.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="login"){
                include "content/login.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="register"){
                include "content/register.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="addcompany"){
                include "content/addcompany.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="vacancies"){
                include "content/vacancies.php";
            }else {
                include "content/home.php";
            }
        ?>
    </div>
    <!-- Footer -->
    <?php include_once 'blocks/footer.php'?>

</body>
</html> 