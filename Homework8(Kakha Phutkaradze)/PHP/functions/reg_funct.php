<?php 
    function captcha(){
        $captcha = "";
        for($i=0; $i<5; $i++){
           $captcha .= rand(0, 9)." ";
        }
        return $captcha;
    }
    $captcha = captcha();
     
    function check_captcha($captcha){ 
        if(str_replace(' ', '', $_POST['real_captcha'])==$captcha){
           return "";
        }else{
           return "<span style='color: red;'>The captcha isn't correct!!</span>";
        }
    }

    $error = "";
    
    if(isset($_POST['submit'])){
        $flname = $_POST['flname'];
        $email = $_POST['email'];
        $pwd = $_POST['pwd'];
        $repwd = $_POST['pwd-repeat'];

        // Password validation
        $uppercase    = preg_match('@[A-Z]@', $pwd);
        $number       = preg_match('@[0-9]@', $pwd);
        $specialChars = preg_match('@[^\w]@', $pwd);

        // Check if email exists
        $selectemail = "SELECT email FROM users where email ='".$email."'";
        $emailCheck = mysqli_query($connect, $selectemail);

        if($pwd == $repwd) {
            $pwd = sha1($salte.$pwd);
            $insert = "INSERT INTO users(flname, email, password) VALUES('$flname', '$email', '$pwd')";

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $error = "Invalid Email Format!";
            }else if(mysqli_num_rows($emailCheck)){
                $error = "This email is already exists!";
            }else if(empty($pwd)){
                $error = "Please enter password";
            }else if(!$uppercase){
                $error = "Password must include at least one Uppercase letter";
            }else if(!$number) {
                $error = "Password must include one number";
            }else if(!$specialChars) {
                $error = "password must include one special chars ex. !@#$%^&*()";
            }else if(strlen($pwd) < 8) {
                $error = "Password must be at least 8 characters in length";
            }else if(!mysqli_query($connect, $insert)){
                die("Error with mysqli insert query!");
            }else{
                header("location: index.php");
            }
        }else{
            $error = "Passwords are not equal!";
        }
    }
?>