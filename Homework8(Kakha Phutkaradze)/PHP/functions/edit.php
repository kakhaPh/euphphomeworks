<?php 
    $id = $_GET['id'];
    $edit = "SELECT * FROM companies WHERE id='$id'";
    if(!mysqli_query($connect, $edit)){
        die("Error with mysqli Select query!!!");
    }else {
        $result = mysqli_query($connect, $edit);
        $record = mysqli_fetch_assoc($result);
    }
?>

<h2 class="title">Edit</h2>

<?php
    $error = "";
    $name = $address = $year = "";

    if(isset($_POST["edit"])) {
        $name = $_POST["name"];
        $address = $_POST["address"];
        $year = $_POST["year"];
        $id = $_POST['id'];
        $edit = "UPDATE companies SET name='$name', address='$address', year='$year' WHERE id='$id'";
        
        if(empty($name)) {
            $error = "Name is required!";
        }else if(strlen($name) < 2 || strlen($name) > 50) {
            $error = "Please enter correct Company name";
        }else if(empty($address)) {
            $error = "Address is required";
        }else if(empty($year)) {
            $error = "Year is required!";
        }else if (!mysqli_query($connect, $edit)) {
            die ("Error with mysql edit query");
        }else if(empty($error)){
            header("location: index.php?menu=companies");
        }
    }
?>


<div class="form_cont">
    <form method="POST">
        <input type="text" name="name" placeholder="Company Name..." value="<?=$record['name']?>">
        <br><br>
        <textarea name="address" cols="24" rows="7"><?=$record['address']?></textarea>
        <br><br>
        <input type="number" name="year" placeholder="Year..." value="<?=$record['year']?>">
        <br><br>
        <input type="hidden" name="id" value="<?=$record['id']?>">
        <input type="submit" name="edit" value="Edit" >
        <br><br>
        <span class="warning"><?=$error?></span>
    </form>
</div>